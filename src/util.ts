/** sets the year of the date passed to current year,
 *  and optionally increases year by one based on the second argument */
const correctYear = (date: number, add = false) => {
	const d = new Date(date);
	d.setFullYear(new Date().getFullYear());
	if (add) d.setFullYear(d.getFullYear() + 1);
	return d.getTime();
};

/**
 * @param birthDate birth date
 * @param userDate user timestamp
 */
export const findDaysBetweenDates = (birthDate: number, userDate: number) => {
	const ONE_DAY = 1000 * 60 * 60 * 24;
	const { from, to } =
		userDate > correctYear(birthDate)
			? { to: correctYear(birthDate, true), from: userDate }
			: { to: userDate, from: correctYear(birthDate) };
	const differenceMs = Math.abs(to - from);
	return Math.floor(differenceMs / ONE_DAY);
};
