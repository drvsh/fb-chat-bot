export const enum States {
	Start,
	Name,
	Birthdate,
	Question,
}

//We can use redis here.
export const Store = new Map<string, States | undefined>();
