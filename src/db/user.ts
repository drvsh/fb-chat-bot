import * as mongoose from "mongoose";

export type UserSchemaType = {
	id: string;
	name: string;
	birthDate: Date;
};

const UserSchema = new mongoose.Schema(
	{
		_id: {
			type: String,
		},
		name: String,
		birthDate: Date,
	},
	{ _id: false, id: false }
);

export const UserModel = mongoose.model<UserSchemaType>("user", UserSchema);
