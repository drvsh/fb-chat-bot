import * as mongoose from "mongoose";

type MessageSchemaType = {
	text: string;
	time: string;
	userId: string;
};

const MessageSchema = new mongoose.Schema({
	text: String,
	time: Number,
	userId: {
		type: String,
		ref: "user",
	},
});

export const MessageModel = mongoose.model<MessageSchemaType>(
	"message",
	MessageSchema
);
