import { config } from "dotenv";
import mongoose from "mongoose";
import express, { type Request } from "express";

// Propogate environment variables
config();

mongoose.connect(process.env.MONGO_URI!);

import { bot } from "./bot/bot";
import { MessageModel } from "./db/message";
import { UserModel } from "./db/user";

const app = express();
app.use(express.json());

const router = bot.initialize();

app.use("/webhook", router);

/**Return all messages received from users*/
app.get("/messages", async (_, response, next) => {
	try {
		const messages = await MessageModel.find({}, { text: 1, id: 1 });
		return response.json(messages);
	} catch (error) {
		return next(error);
	}
});

/**Return single message by its ID*/
app.get(
	"/messages/:id",
	async (request: Request<{ id: string }>, response, next) => {
		try {
			const id = request.params.id;
			const message = await MessageModel.findOne({ id });
			return response.json(message);
		} catch (error) {
			next(error);
		}
	}
);

/** return summary */
app.get("/summary", async (_, response) => {
	const summary = await UserModel.aggregate([
		{
			$lookup: {
				from: "messages",
				localField: "_id",
				foreignField: "userId",
				as: "messages",
			},
		},
		{
			$project: {
				_id: false,
				user: "$_id",
				name: "$name",
				messages: {
					text: true,
					time: true,
				},
			},
		},
	]).exec();
	return response.json(summary);
});

const PORT = Number(process.env.PORT);

app.listen(PORT, () => console.log(`App is running on ${PORT}`));
