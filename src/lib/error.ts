export class BotError extends Error {
	status?: number;
	body?: unknown;
	constructor(message: string, other: { status?: number; body?: unknown }) {
		super(message);
		this.status = other.status;
		this.body = other.body;
	}
}
