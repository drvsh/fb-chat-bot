import { EventEmitter } from "events";
import { type Request, Router } from "express";

import { Payload, Context, BotError, BotConfig, BotEvent } from "../lib";

export class Bot {
	#config: BotConfig;
	#event: BotEvent = new EventEmitter();
	#router = Router();
	constructor(config: BotConfig) {
		this.#config = config;
		if (!this.#config.token || !this.#config.verifyToken) {
			throw new BotError("Please provide a valid token and verifyToken", {});
		}
	}

	on = this.#event.on.bind(this.#event);

	/**
	 * @description This method must be called to start the bot
	 * @returns {Router}
	 * @example
	 * ```
	 * const app = express()
	 * const router = bot.initalize()
	 * app.use("/webhook", router);
	 * app.listen(5000, () => console.log("Bot listening on port 5000"))
	 * ```
	 */
	initialize(): Router {
		this.#router.get("/", (req, res) => {
			const mode = req.query["hub.mode"];
			const token = req.query["hub.verify_token"];
			const challenge = req.query["hub.challenge"];

			const verificationSuccess = mode && token === this.#config.verifyToken;
			if (verificationSuccess) return res.status(200).send(challenge);
			res.sendStatus(403);
			throw new BotError(
				"Webhook verification failed, please check your verification token",
				{}
			);
		});
		this.#router.post("/", (req: Request<never, never, Payload>, res) => {
			for (const entry of req.body.entry) {
				for (const messaging of entry.messaging) {
					this.#event.emit(
						"message",
						new Context(
							messaging,
							this.#config.token,
							this.#event.emit.bind(this.#event)
						)
					);
				}
			}
			return res.sendStatus(200);
		});
		return this.#router;
	}
}
