export type Message = {
	mid: string;
	text: string;
	quick_reply?: { payload: string };
};

export type Messaging<T extends Message> = {
	sender: {
		id: string;
	};
	recipient: {
		id: string;
	};
	timestamp: number;
} & (T extends Message ? { message: Message } : never);

export type Entry = {
	id: string;
	time: number;
	messaging: Messaging<Message>[];
};

export type Payload = {
	entry: Entry[];
};

export type QuickReply = {
	content_type: "text";
	title: string;
	payload: string;
};

export type BotConfig = {
	token: string;
	verifyToken: string;
};
