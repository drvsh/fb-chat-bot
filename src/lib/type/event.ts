import type { BotError, Context, MaybePromise, Message } from "..";

type Events = {
	message: Context<Message>;
	error: BotError;
};

export interface BotEvent {
	on: <EventName extends keyof Events>(
		eventName: EventName,
		contextCallBack: (context: Events[EventName]) => MaybePromise<void>
	) => MaybePromise<this>;
	emit: <EventName extends keyof Events>(
		eventName: EventName,
		data: Events[EventName]
	) => boolean;
}
