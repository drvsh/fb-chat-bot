import axios, { type AxiosError } from "axios";

import {
	type Messaging,
	type QuickReply,
	type Message,
	BotError,
	type BotEvent,
} from ".";

export class Context<T extends Message> {
	#token: string;
	#emitEvent: BotEvent["emit"];
	constructor(
		public message: Messaging<T>,
		token: string,
		emitEvent: BotEvent["emit"]
	) {
		this.#token = token;
		this.#emitEvent = emitEvent;
	}
	async sendMessage(
		message: string,
		extra: {
			quickReplies?: Omit<QuickReply, "content_type">[];
		} = {}
	) {
		return axios
			.post(
				`https://graph.facebook.com/v13.0/me/messages?access_token=${
					this.#token
				}`,
				{
					messaging_type: "RESPONSE",
					recipient: {
						id: this.message.sender.id,
					},
					message: {
						text: message,
						...(extra.quickReplies?.length && {
							quick_replies: extra.quickReplies?.map(
								(reply): QuickReply => ({ ...reply, content_type: "text" })
							),
						}),
					},
				}
			)
			.then((res) => res.data)
			.catch((error: AxiosError) =>
				this.#emitEvent(
					"error",
					new BotError(error.message, {
						body: error.response?.data,
						status: error.response?.status,
					})
				)
			);
	}
}
