import { findDaysBetweenDates } from "../../util";
import { Context, Message } from "../../lib";
import { States, Store } from "../../state";
import type { UserModel } from "../../db/user";
import { MessageModel } from "../../db/message";

export const messageHandler = async (
	ctx: Context<Message>,
	UserRepo: typeof UserModel,
	MessageRepo: typeof MessageModel
) => {
	const userId = ctx.message.sender.id;
	const state = Store.get(userId);

	await MessageRepo.create({
		text: ctx.message.message.text,
		time: ctx.message.timestamp,
		userId,
	});

	// if state is "start" or user is a new user then ask the name
	if (state === States.Start || !state) {
		await ctx.sendMessage("Hello, what's your name?");
		await UserRepo.updateOne(
			{ _id: userId },
			{ _id: userId },
			{ upsert: true }
		);
		Store.set(userId, States.Name);
		return;
	}
	// save the name and ask birthdate
	if (state === States.Name) {
		const name = ctx.message.message.text;
		await UserRepo.updateOne(
			{ _id: userId },
			{
				$set: { name },
			}
		);
		await ctx.sendMessage("What's your birthdate?");
		Store.set(userId, States.Birthdate);
		return;
	}
	//save the birthdate and ask birthday question with quick replies
	if (state === States.Birthdate) {
		const birthDate = new Date(ctx.message.message.text);
		if (Number.isNaN(birthDate.getTime())) {
			await ctx.sendMessage(
				"Invalid date! Please send using YYYY/MM/DD format"
			);
			return;
		}
		await UserRepo.updateOne(
			{ _id: userId },
			{
				$set: { birthDate },
			}
		);
		await ctx.sendMessage(
			"Do you want to know how many days till your next birthday?",
			{
				quickReplies: [
					{ title: "yes", payload: "yes" },
					{ title: "yeah", payload: "yes" },
					{ title: "yup", payload: "yes" },
					{ title: "no", payload: "no" },
					{ title: "nah", payload: "no" },
				],
			}
		);
		Store.set(userId, States.Question);
		return;
	}
	//if user sends yes, answer the question otherwise end the conversation with Goodbye.
	if (state === States.Question) {
		const message = ctx.message.message.quick_reply?.payload;
		const userTime = ctx.message.timestamp;
		if (message === "yes") {
			const userOnDb = await UserRepo.findOne(
				{ _id: userId },
				{ birthDate: 1 }
			);
			const birth = userOnDb?.birthDate!;
			const days = findDaysBetweenDates(birth.getTime(), userTime);
			await ctx.sendMessage(
				`There are ${days} day(s) until your next birthday`
			);
		} else {
			await ctx.sendMessage("Goodbye 👋");
		}
		//Once answered, reset state to "start"
		Store.set(userId, States.Start);
		return;
	}
};
