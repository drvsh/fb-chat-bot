import { Bot } from "../lib";
import { UserModel } from "../db/user";
import { messageHandler } from "./hander/message-handler";
import { MessageModel } from "../db/message";

const bot = new Bot({
	token: process.env.PAGE_TOKEN!,
	verifyToken: process.env.VERIFY_TOKEN!,
});

bot.on("message", (ctx) => messageHandler(ctx, UserModel, MessageModel));

bot.on("error", (error) => {
	console.error({ error });
});

export { bot };
