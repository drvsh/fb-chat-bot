# Demo

![Demo](./demo.gif)


# Dependencies
 - MongoDB


# Usage

Subscribe to only `messages`, `message_postbacks`, and `messaging_customer_information` when setting up webhook.

1. Run `cp example.env .env` and fill in values in `.env`
2.  `npm i && npm start`
3. Download ngrok and run `ngrok http 5000` and use `https` 

## API 

### `GET /messages`

### `GET /messages/:id`

### `GET /summary`
